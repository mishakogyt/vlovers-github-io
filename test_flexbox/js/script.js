$(document).ready(function(){
	$('.slider').slick({
		arrows:false,
		infinite:false,
		slidesToShow:1.5,
		speed:1000,
		responsive:[
			{
				breakpoint: 768,
				settings: {
					slidesToShow:2
				}
			},
			{
				breakpoint: 550,
				settings: {
					slidesToShow:1
				}
			}
		]
	});
});

