import './slider';
import scrolling from './modules/scrolling';
import checkTextInputs from './modules/checkTextInputs';
import mobileMenu from './modules/mobileMenu';
import calc from './modules/calc';








window.addEventListener('DOMContentLoaded', () => {

   scrolling('.up');
   mobileMenu('.burger_menu ', '.header__mob-menu');
   calc();
   
});