const checkTextInputs = (telSelector, nameSelector) => {
   const telInput = document.querySelectorAll(telSelector),
         nameInput = document.querySelectorAll(nameSelector);


      telInput.forEach(input => {
         input.addEventListener('keypress', function (e) {
            if (e.key.match(/[^0-9]/ig)) {
               e.preventDefault();
            }
         });
      });


      nameInput.forEach(input => {
         input.addEventListener('keypress', function (e) {
            if (e.key.match(/[^а-я, a-z]/ig)) {
               e.preventDefault();
            }
         });

         input.addEventListener("input", function () {
            this.value = this.value[0].toUpperCase() + this.value.slice(1);
         });
      });

};

export default checkTextInputs;