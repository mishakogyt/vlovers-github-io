
const mobileMenu = (trigger, content) => {
   const burgerElem = document.querySelector(trigger),
         menu = document.querySelector(content),
         menuLink = menu.querySelectorAll('.header__mob-menu nav ul li'),
         burgerLine = burgerElem.querySelectorAll('div');


   menu.querySelector('.header__mob-menu nav ul').style.display = 'none';
   
   burgerElem.addEventListener('click', () => {
      if (menu.querySelector('.header__mob-menu nav ul').style.display == 'none' && window.screen.availWidth < 725) {
         burgerLine.forEach(item => {
            item.classList.add('active');
         });

         menu.querySelector('.header__mob-menu nav ul').style.display = 'block';
         menu.querySelector('.header__mob-menu .btn-white').style.display = 'block';
         menuLink.forEach(link => {
            link.classList.remove('animated', 'fadeOutUp');
            link.classList.add('animated', 'fadeInUp');
         });

         // menuLink[0].setAttribute("data-wow-delay", "6s");
         // menuLink[1].setAttribute("data-wow-delay", "9s");
         // menuLink[2].setAttribute("data-wow-delay", "12s");
         // menuLink[3].setAttribute("data-wow-delay", "16s");

         menu.style.cssText = "-webkit-backdrop-filter: blur(10px); backdrop-filter: blur(10px);background: rgba(101,101,101,.4); transition: 0.6s;";
         document.body.style.overflow = 'hidden';
         document.querySelector('html').style.overflow = 'hidden';


      } else{
          burgerLine.forEach(item => {
             item.classList.remove('active');
          });
         menuLink.forEach(link => {
            link.classList.remove('animated', 'wow', 'fadeInUp');
            link.classList.add('animated', 'wow', 'fadeOutUp');
         });
         
         menu.style.cssText = "height: 10vh; -webkit-backdrop-filter: none; backdrop-filter:none;background: none; transition: 0.6s;";
         document.body.style.overflow = '';
         document.querySelector('html').style.overflow = '';
         setTimeout(() => {
            menu.querySelector('.header__mob-menu nav ul').style.display = 'none';
            menu.querySelector('.header__mob-menu .btn-white').style.display = 'none';

         }, 200);

      }

      menuLink.forEach(link => {
         link.addEventListener('click', () => {
            burgerLine.forEach(item => {
               item.classList.remove('active');
            });
            
            link.classList.remove('animated', 'wow', 'fadeInUp');
            link.classList.add('animated', 'wow', 'fadeOutUp');
            menu.style.cssText = "height: 10vh; -webkit-backdrop-filter: none; backdrop-filter:none;background: none; transition: 0.6s;";
            document.body.style.overflow = '';
            document.querySelector('html').style.overflow = '';
            setTimeout(() => {
               menu.querySelector('.header__mob-menu nav ul').style.display = 'none';
               menu.querySelector('.header__mob-menu .btn-white').style.display = 'none';

            }, 300);
         });
      })
   });

};

export default mobileMenu;